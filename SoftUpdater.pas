unit SoftUpdater;

interface

uses
  System.SysUtils,
  windows,
  IdStack,
  Vcl.Forms,
  Vcl.Dialogs,
  ShellApi,
  DownloadThread,
  System.IOUtils,
  updaterClasses,
  System.Classes,
  IdHTTP,
  System.Json,
  IdHashMessageDigest,
  Generics.Collections;

type
  TOnGetUpdates = procedure(Sender: TObject) of Object;
  TOnDownloadFile = procedure(Sender: TObject; AMemoryStream: TMemoryStream; AVersionFile: TVersionFile) of Object;
  TOnFinishDownload = procedure(Sender: TObject) of Object;
  TOnError = TNotifyEvent;

type
  TSoftUpdater = class(TComponent)
  private
    FVersion: string;
    FHost: string;
    FSystemName: string;
    IdHTTP1: TIdHTTP;
    FOnGetUpdates: TOnGetUpdates;
    FOnDownloadFile: TOnDownloadFile;
    FOnFinishDownload: TOnFinishDownload;
    LeftFiles: integer;
    FRestartIfNeed: boolean;
    FApiKey: string;
    FSaveBakFiles: boolean;
    FProxyPort: integer;
    FProxyHost: string;
    FGetNextVersion: boolean;
    FChangeLog: string;
    FOnError: TOnError;
    procedure SetVersion(const Value: string);
    procedure SetHost(const Value: string);
    procedure SetSystemName(const Value: string);
    procedure SetOnGetUpdates(const Value: TOnGetUpdates);
    procedure SetOnDownloadFile(const Value: TOnDownloadFile);
    procedure SetOnFinishDownload(const Value: TOnFinishDownload);
    function GetPath(AVersionFile: TVersionFile; AFromApplication: boolean = false): string;
    procedure SetRestartIfNeed(const Value: boolean);
    procedure SetApiKey(const Value: string);
    function GetFileMd5(AFilePath: string): string;
    procedure SetSaveBakFiles(const Value: boolean);
    function SaveBakFile(AVersionFile: TVersionFile): boolean;
    function RestoreBakFile(AVersionFile: TVersionFile): boolean;
    function Restore(): boolean;
    function ReplaceFile(AVersionFile: TVersionFile): boolean;
    function DeleteBakFile(AVersionFile: TVersionFile): boolean;
    procedure SetProxyHost(const Value: string);
    procedure SetProxyPort(const Value: integer);
    function GetProxyHost: string;
    function GetProxyPort: integer;
    procedure SetGetNextVersion(const Value: boolean);
    procedure SetChangeLog(const Value: string);
    function GetChangeLog: string;
    procedure SetOnError(const Value: TOnError);
    { Private declarations }
  protected
    { Protected declarations }
    procedure OnDownloadFileDefault(Sender: TObject; AMemoryStream: TMemoryStream; AVersionFile: pointer);
  public
    { Public declarations }
    FUpdates: TVersionData;
    Downloaded: boolean;
    function GetLastVersion(): string;
    function GetContent(AURL: string): TJsonAnswer;
    function CheckUpdates(Sender: TObject): boolean;
    function Download(): TList<TMemoryStream>;
    procedure Install();
    function HaveUpdates(): boolean;
    procedure Restart;
    property ChangeLog: string read GetChangeLog write SetChangeLog;
  published
    { Published declarations }
    property SaveBakFiles: boolean read FSaveBakFiles write SetSaveBakFiles;
    property RestartIfNeed: boolean read FRestartIfNeed write SetRestartIfNeed;
    property OnGetUpdates: TOnGetUpdates read FOnGetUpdates write SetOnGetUpdates;
    property OnDownloadFile: TOnDownloadFile read FOnDownloadFile write SetOnDownloadFile;
    property OnFinishDownload: TOnFinishDownload read FOnFinishDownload write SetOnFinishDownload;
    property Host: string read FHost write SetHost;
    property ProxyHost: string read GetProxyHost write SetProxyHost;
    property ProxyPort: integer read GetProxyPort write SetProxyPort;
    property Version: string read FVersion write SetVersion;
    property SystemName: string read FSystemName write SetSystemName;
    property ApiKey: string read FApiKey write SetApiKey;
    property GetNextVersion: boolean read FGetNextVersion write SetGetNextVersion default false;
    property OnError: TOnError read FOnError write SetOnError;
    constructor Create(AOwner: TComponent); override;
    destructor Detroy();
  end;

const
  UPD_TMP_EXTENSION = 'upd~';

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('System', [TSoftUpdater]);
end;

{ TUpdater }

function TSoftUpdater.CheckUpdates(Sender: TObject): boolean;
var
  F: TJsonAnswer;
begin
  if SystemName.IsEmpty then
    Exception.Create('Set SystemName');
  F := GetContent('api/project/' + SystemName + '/updates?version=' + Version);
  FUpdates := TVersionData.Create(F);
  if (F.Code <> -1) and Assigned(FOnGetUpdates) then
    FOnGetUpdates(Sender);
end;

constructor TSoftUpdater.Create(AOwner: TComponent);
begin
  inherited;
  FHost := 'http://appsun.ru';
  IdHTTP1 := TIdHTTP.Create(self);
  IdHTTP1.ProxyParams.Clear;
end;

function TSoftUpdater.DeleteBakFile(AVersionFile: TVersionFile): boolean;
var
  BakFile1: string;
begin
  BakFile1 := GetPath(AVersionFile) + '.bak';
  Result := DeleteFile(PChar(BakFile1));
end;

destructor TSoftUpdater.Detroy;
begin
  FreeAndNil(IdHTTP1);
end;

function TSoftUpdater.GetChangeLog: string;
begin
  // if FChangeLog = '' then
  // begin
  // GetContent('api/version' + SystemName + '/about');
  // end
  // else
  // Result := FChangeLog;
end;

function TSoftUpdater.GetContent(AURL: string): TJsonAnswer;
var
  Content1: string;
begin
  Content1 := '';
  IdHTTP1.Disconnect;
  try
    Content1 := IdHTTP1.Get(Host + '/' + AURL);
  except
    on E: EIdSocketError do
      Content1 := '';
    on E: Exception do
      Content1 := '';
  end;
  Result := TJsonAnswer.Create(Content1);
  if (Result.Code <> 0) and (Assigned(FOnError)) then
    FOnError(self);
end;

function TSoftUpdater.GetFileMd5(AFilePath: string): string;
var
  IdMD5: TIdHashMessageDigest5;
  FS: TFileStream;
begin
  IdMD5 := TIdHashMessageDigest5.Create;
  FS := TFileStream.Create(AFilePath, fmOpenRead or fmShareDenyWrite);
  try
    Result := IdMD5.HashStreamAsHex(FS)
  finally
    FS.Free;
    IdMD5.Free;
  end;
end;

function TSoftUpdater.GetLastVersion: string;
var
  AData: TJsonAnswer;
begin
  AData := GetContent('api/project/' + self.SystemName + '/next-version');
  Result := AData.Data.GetValue<string>('value');
end;

function TSoftUpdater.GetPath(AVersionFile: TVersionFile; AFromApplication: boolean): string;
var
  folder1: string;
begin
  if (AVersionFile.Folder = '') then
    folder1 := ''
  else
    folder1 := AVersionFile.Folder + '\';
  if (AFromApplication) then
    Result := ExtractFileDir(ParamStr(0)) + '\' + folder1 + AVersionFile.Name
  else
    Result := GetHomePath() + '\Appsun Software Updates' + '\' + SystemName + '\' + folder1 + AVersionFile.Name;
end;

function TSoftUpdater.GetProxyHost: string;
begin
  Result := IdHTTP1.ProxyParams.ProxyServer;
end;

function TSoftUpdater.GetProxyPort: integer;
begin
  Result := IdHTTP1.ProxyParams.ProxyPort;
end;

function TSoftUpdater.HaveUpdates: boolean;
begin
  if Assigned(FUpdates) then
  begin
    Result := false;
    if Assigned(FUpdates.Files) then
      Result := FUpdates.Files.Count > 0;
  end
  else
    Result := false;
end;

procedure TSoftUpdater.OnDownloadFileDefault(Sender: TObject; AMemoryStream: TMemoryStream; AVersionFile: pointer);
var
  folder1: string;
  FileName1: string;
  FileStream1: TFileStream;
  VersionFile1: TVersionFile;
begin
  VersionFile1 := TVersionFile(AVersionFile);
  folder1 := ExtractFileDir(GetPath(VersionFile1));
  if not DirectoryExists(folder1) then
    ForceDirectories(folder1);
  if Assigned(FOnDownloadFile) then
    FOnDownloadFile(self, AMemoryStream, VersionFile1);
  FileName1 := GetPath(VersionFile1);
  FileStream1 := TFileStream.Create(FileName1, fmCreate);
  AMemoryStream.Position := 0;
  FileStream1.CopyFrom(AMemoryStream, AMemoryStream.Size);
  FileStream1.Free;
  inc(LeftFiles, -1);
  if (LeftFiles = 0) then
  begin
    Downloaded := true;
    if Assigned(FOnFinishDownload) then
      FOnFinishDownload(self);
  end;
end;

function TSoftUpdater.ReplaceFile(AVersionFile: TVersionFile): boolean;
var
  ApplicationFile1: string;
  NewFile1: string;
  ApplicationDir1: string;
begin
  ApplicationFile1 := GetPath(AVersionFile, true);
  ApplicationDir1 := ExtractFileDir(ApplicationFile1);
  NewFile1 := GetPath(AVersionFile);
  Result := false;
  if not DirectoryExists(ApplicationDir1) then
    ForceDirectories(ApplicationDir1);
  if ((not FileExists(ApplicationFile1)) and FileExists(NewFile1)) then
    RenameFile(NewFile1, ApplicationFile1);
  Result := FileExists(ApplicationFile1);
  if (not SaveBakFiles and Result) then
    DeleteBakFile(AVersionFile);
end;

procedure TSoftUpdater.Restart;
begin
  if (FileExists(Application.ExeName)) then
  begin
    ShellExecute(0, nil, PChar(Application.ExeName), '--no-check-mutex --show-minimized', nil, SW_SHOWNORMAL);
    Application.Terminate;
  end;
end;

function TSoftUpdater.Restore: boolean;
var
  i: integer;
  s: string;
  NewFilePath1, OldFilePath1, BakFile1: string;
  VersionFile1: TVersionFile;
begin
  for i := 0 to FUpdates.Files.Count - 1 do
    RestoreBakFile(FUpdates.Files.Items[i]);
end;

function TSoftUpdater.RestoreBakFile(AVersionFile: TVersionFile): boolean;
var
  ApplicationFile1: string;
  BakFile1: string;
  TmpFile1: string;
begin
  ApplicationFile1 := GetPath(AVersionFile, true);
  BakFile1 := GetPath(AVersionFile) + '.bak';
  if (FileExists(ApplicationFile1)) then
    DeleteFile(PChar(ApplicationFile1));
  if FileExists(BakFile1) then
  begin
    RenameFile(BakFile1, ApplicationFile1);
    Result := FileExists(ApplicationFile1);
  end
  else
    Result := false;
end;

function TSoftUpdater.SaveBakFile(AVersionFile: TVersionFile): boolean;
var
  ApplicationFile1: string;
  BakFile1: string;
  TmpFile1: string;
begin
  ApplicationFile1 := GetPath(AVersionFile, true);
  BakFile1 := GetPath(AVersionFile) + '.bak';
  if FileExists(BakFile1) then
    DeleteFile(PChar(BakFile1));
  Result := RenameFile(ApplicationFile1, BakFile1);
  if (FileExists(ApplicationFile1)) then
  begin
    TmpFile1 := ApplicationFile1 + '.' + UPD_TMP_EXTENSION;
    if (FileExists(TmpFile1)) then
      DeleteFile(PChar(TmpFile1));
    Result := RenameFile(ApplicationFile1, TmpFile1);
  end;
end;

procedure TSoftUpdater.SetApiKey(const Value: string);
begin
  FApiKey := Value;
end;

procedure TSoftUpdater.SetChangeLog(const Value: string);
begin
  FChangeLog := Value;
end;

procedure TSoftUpdater.SetGetNextVersion(const Value: boolean);
var
  AData: TJsonAnswer;
  Value1: string;
begin
  if Value then
  begin
    AData := GetContent('api/project/' + self.SystemName + '/next-version');
    try
      if (AData.Code = 0) then
      begin
        Value1 := AData.Data.GetValue<string>('value');
        FVersion := Value1;
        ShowMessage('Avaliable next version ' + Value1);
      end
      else
        ShowMessage('Next version not found');
    except
      ShowMessage('Fail get next version');
    end;
  end;
end;

procedure TSoftUpdater.SetHost(const Value: string);
begin
  FHost := Value;
end;

procedure TSoftUpdater.SetOnDownloadFile(const Value: TOnDownloadFile);
begin
  FOnDownloadFile := Value;
end;

procedure TSoftUpdater.SetOnError(const Value: TOnError);
begin
  FOnError := Value;
end;

procedure TSoftUpdater.SetOnFinishDownload(const Value: TOnFinishDownload);
begin
  FOnFinishDownload := Value;
end;

procedure TSoftUpdater.SetOnGetUpdates(const Value: TOnGetUpdates);
begin
  FOnGetUpdates := Value;
end;

procedure TSoftUpdater.SetProxyHost(const Value: string);
begin
  IdHTTP1.ProxyParams.ProxyServer := Value;
end;

procedure TSoftUpdater.SetProxyPort(const Value: integer);
begin
  IdHTTP1.ProxyParams.ProxyPort := Value;
end;

procedure TSoftUpdater.SetRestartIfNeed(const Value: boolean);
begin
  FRestartIfNeed := Value;
end;

procedure TSoftUpdater.SetSaveBakFiles(const Value: boolean);
begin
  FSaveBakFiles := Value;
end;

procedure TSoftUpdater.SetSystemName(const Value: string);
begin
  FSystemName := Value;
end;

procedure TSoftUpdater.SetVersion(const Value: string);
begin
  FVersion := Value;
end;

procedure TSoftUpdater.Install;
var
  i: integer;
  s: string;
  NewFilePath1, OldFilePath1, BakFile1: string;
  VersionFile1: TVersionFile;
begin
  for i := 0 to FUpdates.Files.Count - 1 do
  begin
    VersionFile1 := FUpdates.Files.Items[i];
    NewFilePath1 := GetPath(VersionFile1);
    OldFilePath1 := GetPath(VersionFile1, true);
    if (FileExists(NewFilePath1)) then
    begin
      SaveBakFile(VersionFile1);
      if (not self.ReplaceFile(VersionFile1)) then
      begin
        Restore();
        exit;
      end;
    end;
  end;
  Downloaded := false;
  if RestartIfNeed and FUpdates.NeedRestart then
    Restart;
end;

function TSoftUpdater.Download: TList<TMemoryStream>;
var
  MemoryStream1: TMemoryStream;
  i: integer;
  Thread1: TDownloadThread;
  VersionFile1: TVersionFile;
  NewPath1: string;
  OldPath1: string;
  Flag1: boolean;
begin
  Downloaded := false;
  Flag1 := false;
  LeftFiles := FUpdates.Files.Count;
  for i := 0 to FUpdates.Files.Count - 1 do
  begin
    VersionFile1 := FUpdates.Files.Items[i];
    NewPath1 := GetPath(VersionFile1);
    OldPath1 := GetPath(VersionFile1, true);
    if (FileExists(OldPath1)) then
      if (VersionFile1.Md5.ToUpper = GetFileMd5(OldPath1)) then
      begin
        inc(LeftFiles, -1);
        continue;
      end;
    if (FileExists(NewPath1)) then
      if (VersionFile1.Md5.ToUpper = GetFileMd5(NewPath1)) then
      begin
        inc(LeftFiles, -1);
        continue;
      end;
    Flag1 := true;
    Thread1 := TDownloadThread.Create(VersionFile1.Url);
    Thread1.IdHTTP.ProxyParams.Assign(IdHTTP1.ProxyParams);
    Thread1.Data := VersionFile1;
    Thread1.OnFinish := self.OnDownloadFileDefault;
    Thread1.Resume;
  end;
  if (not Flag1) and (LeftFiles = 0) and Assigned(FOnFinishDownload) then
  begin
    Downloaded := true;
    FOnFinishDownload(self);
  end;
end;

end.
