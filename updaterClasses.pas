unit updaterClasses;

interface

uses System.Json,
  System.SysUtils,
  Generics.Collections, System.Classes, IdHTTP;

type
  TVersionFile = class(TObject)
    Execute: boolean;
    Folder: string;
    Md5: string;
    Name: string;
    Size: integer;
    URL: string;
    constructor Create(content: string);
  end;

type
  TJsonAnswer = class(TObject)
  public
    Code: integer;
    Data: TJSONValue;
    Message: string;
    constructor Create(AContent: string);
    destructor Destroy();
  end;

type
  TVersionData = class(TObject)
    NeedConfirmLicense: boolean;
    NeedReboot: boolean;
    NeedRestart: boolean;
    Size: integer;
    VersionType: string;
    Version: string;
    ChangeLog: string;
    Files: TList<TVersionFile>;
    constructor Create(AJson: TJsonAnswer);
    destructor Destroy(); override;
  end;

implementation

{ TJsonAnswer }

constructor TJsonAnswer.Create(AContent: string);
var
  Json: TJSONValue;
begin
  try
    Json := TJSONObject.ParseJSONValue(AContent);
    Code := Json.GetValue<integer>('code');
    if (Code = 0) then
      Data := Json.GetValue<TJSONValue>('data')
    else
      Message := Json.GetValue<string>('message');
  except
    on E: Exception do
    begin
      Code := -1;
      Message := 'Unknown error';
    end;
  end;
end;

destructor TJsonAnswer.Destroy;
begin
  Data.Free;
end;

{ TVersionData }

constructor TVersionData.Create(AJson: TJsonAnswer);
var
  JsonFilesData: TJSONArray;
  i: integer;
  VersionFile: TVersionFile;
begin
  Files := TList<TVersionFile>.Create;
  NeedConfirmLicense := false;
  ChangeLog := '';
  NeedReboot := false;
  NeedRestart := false;
  VersionType := '';
  Version := '';
  Size := 0;
  if (AJson.Code <> 0) then
    exit;
  try
    with AJson.Data do
    begin
      NeedConfirmLicense := GetValue<boolean>('need_confirm_license');
      NeedReboot := GetValue<boolean>('need_reboot');
      NeedRestart := GetValue<boolean>('need_restart');
      ChangeLog := GetValue<string>('change_log');
      Size := GetValue<integer>('size');
      VersionType := GetValue<string>('type');
      Version := GetValue<string>('version');
      JsonFilesData := GetValue<TJSONArray>('files');
      for i := 0 to JsonFilesData.Count - 1 do
      begin
        VersionFile := TVersionFile.Create(JsonFilesData.Items[i].ToString);
        Files.Add(VersionFile);
      end;
    end;
  except

  end;
end;

destructor TVersionData.Destroy;
begin
  Files.Free;
  inherited;
end;

{ TVersionFile }

constructor TVersionFile.Create(content: string);
var
  Json: TJSONValue;
begin
  Json := TJSONObject.ParseJSONValue(content);
  Execute := Json.GetValue<boolean>('execute');
  Folder := Json.GetValue<string>('folder');
  Md5 := Json.GetValue<string>('md5');
  Name := Json.GetValue<string>('name');
  Size := Json.GetValue<integer>('size');
  URL := Json.GetValue<string>('url');
end;

end.
